# Surfing.MethodCall

#### 介绍
 MethodCall是实现rpc的核心设计思路，例子里包含GRPC的完整实例（是参考GRPC实现的）。Thrift的是写了一半的。
 核心代码在Core中，可以抽离出来用于实现RPC，里面带了 Interceptor
 还有代码生成端在写。

 最希望能实现为 批量的rpc调用。


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)