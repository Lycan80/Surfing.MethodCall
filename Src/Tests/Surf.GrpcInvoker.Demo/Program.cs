﻿using Surf.GrpcInvoker.Demo.Core;
using Surf.GrpcInvoker.Demo.Demo;
using System;
using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = ServerInitialize();

            // Test
            Test_Call(server);

            Console.ReadLine();
        }

        static Server ServerInitialize()
        {
            var userService = new UserService();

            var server = new Server
            {
                Services = { UserServiceGrpc.BindService(userService).Intercept(new SurfInterceptor()) },
            };
            return server;
        }


        static void Test_Call(Server server)
        {
            var request = new UserRequest() { Id = 5 };

            var user = server.ImitateCall<UserRequest, User>("UserService.GetUser", request).Result;
            Console.WriteLine(user.Name);
        }
    }
}
