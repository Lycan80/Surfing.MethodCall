﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo.Demo
{
    public class UserService
    {
        public Task<User> GetUser(UserRequest request)
        {
            var user = new User() { Id = request.Id, Name = request.Id + "小明" };
            return Task.FromResult(user);
        }
    }

    public class UserRequest
    {
        public int Id { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
