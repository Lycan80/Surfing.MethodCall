﻿using Surf.GrpcInvoker.Demo.Core;
using Surf.GrpcInvoker.Demo.Core.Interceptors;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo.Demo
{
    public class SurfInterceptor : Interceptor
    {
        public virtual Task<TResponse> ServerCallHandler<TRequest, TResponse>(TRequest request, ServerMethod<TRequest, TResponse> continuation)
        {
            Console.WriteLine("Im surf interceptor! Im Started");
            return continuation(request);
        }
    }
}
