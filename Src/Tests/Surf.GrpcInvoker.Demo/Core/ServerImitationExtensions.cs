﻿using Newtonsoft.Json;
using Surf.GrpcInvoker.Demo.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo.Core
{
    public static class ServerImitationExtensions
    {
        private static byte[] Serialize(object obj)
        {
            var data = JsonConvert.SerializeObject(obj);
            return Encoding.ASCII.GetBytes(data);
        }

        public static Task<TResponse> ImitateCall<TRequest, TResponse>(this Server server, string methodName, TRequest message)
        {
            byte[] byteMessage = Serialize(message);
            return (Task<TResponse>)server.ImitateCall(methodName, byteMessage);
        }
    }
}
