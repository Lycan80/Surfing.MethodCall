﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo.Core.Interceptors
{
    public interface Interceptor
    {
        Task<TResponse> ServerCallHandler<TRequest, TResponse>(TRequest request, ServerMethod<TRequest, TResponse> continuation);
    }
}
