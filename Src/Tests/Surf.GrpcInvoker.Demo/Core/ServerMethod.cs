﻿using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo.Core
{
    /// <summary>服务端 方法委托，用于接收被 调用的方法</summary>
    public delegate Task<TResponse> ServerMethod<TRequest, TResponse>(TRequest request);
}
