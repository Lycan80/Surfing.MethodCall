﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Surf.GrpcInvoker.Demo.Core.Interceptors;

namespace Surf.GrpcInvoker.Demo.Core.CallHandlers
{
    internal class EmptyCallHandler : IServerCallHandler
    {
        public static readonly EmptyCallHandler Instance = new EmptyCallHandler();

        public EmptyCallHandler() { }

        public Task HandleCall(byte[] requestMessage)
        {
            return Task.CompletedTask;
        }

        public IServerCallHandler Intercept(Interceptor interceptor)
        {
            return this;
        }
    }
}
