﻿using Surf.GrpcInvoker.Demo.Core.Interceptors;
using System.Threading.Tasks;

namespace Surf.GrpcInvoker.Demo.Core.CallHandlers
{
    /// <summary>方法调用器</summary>
    internal interface IServerCallHandler
    {
        Task HandleCall(byte[] requestMessage);

        // 拦截
        IServerCallHandler Intercept(Interceptor interceptor);
    }
}
