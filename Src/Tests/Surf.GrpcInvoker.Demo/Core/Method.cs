﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Surf.GrpcInvoker.Demo.Core
{
    /// <summary>调用的方法信息</summary>
    public interface IMethod
    {
        string EntityName { get; }


        string Name { get; }


        string FullName { get; }
    }

    /// <summary>调用的方法信息</summary>
    public class Method<TRequest, TResponse> : IMethod
    {
        readonly string _entityName;
        readonly string _name;
        readonly Marshaller<TRequest> _requestMarshaller;
        readonly Marshaller<TResponse> _responseMarshaller;
        readonly string _fullName;


        public Method(string entityName, string name, Marshaller<TRequest> requestMarshaller, Marshaller<TResponse> responseMarshaller)
        {
            _entityName = entityName ?? throw new ArgumentNullException(nameof(entityName));
            _name = name ?? throw new ArgumentNullException(nameof(name));
            _requestMarshaller = requestMarshaller ?? throw new ArgumentNullException(nameof(requestMarshaller));
            _responseMarshaller = responseMarshaller ?? throw new ArgumentNullException(nameof(responseMarshaller));
            _fullName = $"{entityName}.{name}";
        }

        public string EntityName
        {
            get
            {
                return _entityName;
            }
        }

        /// <summary>
        /// Gets the unqualified name of the method.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }
        }

        /// <summary>
        /// Gets the marshaller used for request messages.
        /// </summary>
        public Marshaller<TRequest> RequestMarshaller
        {
            get
            {
                return _requestMarshaller;
            }
        }

        /// <summary>
        /// Gets the marshaller used for response messages.
        /// </summary>
        public Marshaller<TResponse> ResponseMarshaller
        {
            get
            {
                return _responseMarshaller;
            }
        }

        /// <summary>
        /// Gets the fully qualified name of the method. On the server side, methods are dispatched
        /// based on this name.
        /// </summary>
        public string FullName
        {
            get
            {
                return _fullName;
            }
        }
    }
}
