﻿using System;

namespace Surf.Thriftinvoker.Demo.Thrift.Transport
{
    /// <summary>服务端通讯器 定义</summary>
    public abstract class TServerTransport
    {
        public abstract void Listen();
        public abstract void Close();

        protected abstract TTransport AcceptImpl();

        public TTransport Accept()
        {
            TTransport transport = AcceptImpl();
            if (transport == null)
                throw new Exception("accept() way not return NULL");

            return transport;
        }
    }
}
