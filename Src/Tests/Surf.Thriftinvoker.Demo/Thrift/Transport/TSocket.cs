﻿using System;
using System.Net.Sockets;

namespace Surf.Thriftinvoker.Demo.Thrift.Transport
{
    public class TSocket : TStreamTransport
    {
        private TcpClient client = null;
        private string host = null;
        private int port = 0;
        private int timeout = 0;

        public TSocket(TcpClient client)
        {
            this.client = client;

            if (IsOpen)
            {
                inputStream = client.GetStream();
                outputStream = client.GetStream();
            }
        }

        public TSocket(string host, int port)
            : this(host, port, 0)
        {
        }

        public TSocket(string host, int port, int timeout)
        {
            this.host = host;
            this.port = port;
            this.timeout = timeout;

            InitSocket();
        }

        private void InitSocket()
        {
            this.client = CreateTcpClient();
            this.client.ReceiveTimeout = client.SendTimeout = timeout;
            this.client.Client.NoDelay = true;
        }

        #region Utilities
        internal static TcpClient CreateTcpClient()
        {
            TcpClient client = null;
#if NET45
            client = new TcpClient(AddressFamily.InterNetworkV6);
            client.Client.DualMode = true;
#else
            client = new TcpClient(AddressFamily.InterNetwork);
#endif

            return client;
        }

        internal static TcpListener CreateTcpListener(Int32 port)
        {
            TcpListener listener = null;

#if NET45
            listener = new TcpListener(System.Net.IPAddress.IPv6Any, port);
            listener.Server.DualMode = true;
#else

            listener = new TcpListener(System.Net.IPAddress.Any, port);
#endif

            return listener;
        }
        #endregion

        public int Timeout
        {
            set
            {
                client.ReceiveTimeout = client.SendTimeout = timeout = value;
            }
        }

        public TcpClient TcpClient
        {
            get
            {
                return client;
            }
        }

        public string Host
        {
            get
            {
                return host;
            }
        }

        public int Port
        {
            get
            {
                return port;
            }
        }

        public override bool IsOpen
        {
            get
            {
                if (client == null)
                {
                    return false;
                }

                return client.Connected;
            }
        }

        public override void Open()
        {
            if (IsOpen)
            {
                throw new Exception("Socket already connected");
            }

            if (string.IsNullOrEmpty(host))
            {
                throw new Exception("Cannot open null host");
            }

            if (port <= 0)
            {
                throw new Exception("Cannot open without port");
            }

            if (client == null)
            {
                InitSocket();
            }

            if (timeout == 0)            // no timeout -> infinite
            {
                client.Connect(host, port);
            }
            else                        // we have a timeout -> use it
            {
                ConnectHelper hlp = new ConnectHelper(client);
                IAsyncResult asyncres = client.BeginConnect(host, port, new AsyncCallback(ConnectCallback), hlp);
                bool bConnected = asyncres.AsyncWaitHandle.WaitOne(timeout) && client.Connected;
                if (!bConnected)
                {
                    lock (hlp.Mutex)
                    {
                        if (hlp.CallbackDone)
                        {
                            asyncres.AsyncWaitHandle.Close();
                            client.Close();
                        }
                        else
                        {
                            hlp.DoCleanup = true;
                            client = null;
                        }
                    }
                    throw new Exception("Connect timed out");
                }
            }

            inputStream = client.GetStream();
            outputStream = client.GetStream();
        }


        static void ConnectCallback(IAsyncResult asyncres)
        {
            ConnectHelper hlp = asyncres.AsyncState as ConnectHelper;
            lock (hlp.Mutex)
            {
                hlp.CallbackDone = true;

                try
                {
                    if (hlp.Client.Client != null)
                        hlp.Client.EndConnect(asyncres);
                }
                catch (Exception)
                {
                    // catch that away
                }

                if (hlp.DoCleanup)
                {
                    try
                    {
                        asyncres.AsyncWaitHandle.Close();
                    }
                    catch (Exception) { }

                    try
                    {
                        if (hlp.Client is IDisposable)
                            ((IDisposable)hlp.Client).Dispose();
                    }
                    catch (Exception) { }
                    hlp.Client = null;
                }
            }
        }

        private class ConnectHelper
        {
            public object Mutex = new object();
            public bool DoCleanup = false;
            public bool CallbackDone = false;
            public TcpClient Client;
            public ConnectHelper(TcpClient client)
            {
                Client = client;
            }
        }

        public override void Close()
        {
            base.Close();
            if (client != null)
            {
                client.Close();
                client = null;
            }
        }

        #region " IDisposable Support "
        private bool _IsDisposed;

        // IDisposable
        protected override void Dispose(bool disposing)
        {
            if (!_IsDisposed)
            {
                if (disposing)
                {
                    if (client != null)
                        ((IDisposable)client).Dispose();
                    base.Dispose(disposing);
                }
            }
            _IsDisposed = true;
        }
        #endregion
    }
}
